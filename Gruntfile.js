module.exports = function (grunt) {

    'use strict';

    // Configurable paths
    var config = {
        dev: {
            root: 'src',
        },
        dist: {
            root: 'dist',
        }
    };

    // Define the configuration for all the tasks
    grunt.config.init({ config: config });

    // Load all our tasks from dir ./grunt
    grunt.loadTasks('grunt');

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    grunt.registerTask('build', [
        'clean:dist',             // Clear our current dist directory before new build
        'browserify:client',      // create browserify bundles
        'uglify',                 // Configured by useminPrepare task, compile non-main.js JS, e.g. ie8.js (main.js by requirejs below)
        'copy:normalize',         // Copy the normalize scss files to libs directory
        'sass',                   // Compile scss
        'copy:client'             // Copy essential files to dist directory
    ]);

    // Run unit tests
    grunt.registerTask('test', ['mochaTest']);

    // Run browserify tasks
    grunt.registerTask('bsfy', function(target) {
        if (target === 'watch') {
            grunt.task.run(['browserify:clientWatch']);
        } else {
            grunt.task.run(['browserify:client']);
        }
    });

    // Set default task
    grunt.registerTask('default', ['build']);
};
