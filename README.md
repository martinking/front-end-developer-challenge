# Arqiva Front End Developer Challenge #

* * *

This project is used as a technical challenge for prospective front end developers.

Outlined below are the build tasks and the objectives for the challenge.

We suggest you spend 2 - 4 hours to complete the challenge below, but don't let us stop you from spending more time if you like, that said, we appreciate time is precious so if you struggle to find the time to complete all the tasks we'd still like to see your code.

* * *
**FORK THIS REPOSITORY INTO A PRIVATE BITBUCKET REPOSITORY**

Once you've done that add the following user: recruitment@capablue.com as we're keen to see how you work :)
* * *

### How do I get set up? ###

***Prerequisites***

* Node
* Grunt CLI
* SASS

> npm install

### How do I build the project? ###
> grunt

### How do I test? ###
> grunt test

### How do I run the app? ###
Serve or open ./dist/index.html


## What's the challenge? ##

###TASKS###

+ Populate the div.instructions-keypad with a graphical keypad
+ Add 'T9' functionality to the form 'inputs'
+ Select between inputs with up and down cursors
+ Delete letters from inputs with backspace and left cursor
+ Enter must select the next input and then when no more inputs should submit the form
+ Submit contents of inputs to the validate method of the login service module
+ Submission of 'Arqiva' & 'N3wm@nStr33t' should trigger onSuccess callback any other input should trigger onError callback
+ Include unit tests for at least 2 of your classes/modules.

###RULES###

+ **NO** Edits may directly be made to the index.html
+ Input divs **MUST NOT** directly accept keyboard entry
+ Only 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, cursors, enter and backspace may be used to interact with the input fields


###EXTRA POINTS###

+ Update login service to use promises
+ Don't just commit your solution all at once - we want to see your commit history
+ Unit tests for all modules and methods
+ Add a blinking cursor
+ Adding a 'function not allowed' notification for any keyboard input outside scope
+ Adding minify tasks, source maps etc and general optimisation of development workflow
+ Following the [SMACSS](https://smacss.com/) CSS Rules

###END PRODUCT###

[View an image of solution](http://recruitment.capablue.com/frontend/front-end-challenge.png)

[View a video of solution](http://recruitment.capablue.com/frontend/front-end-challenge-solution.mov)

###NUMPAD KEY MAPPING###
+ 1) . , - ? ! ' : ; / 1
+ 2) a b c A B C 2
+ 3) d e f D E F 3
+ 4) g h i G H I 4
+ 5) j k l J K L 5
+ 6) m n o M N O 6
+ 7) p q r s P Q R S 7
+ 8) t u v T U V 8
+ 9) w x y z W X Y Z 0
+ 0) & @ _ 0

* * *

© Copyright 2016 Arqiva Limited. All Rights Reserved.