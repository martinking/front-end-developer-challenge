// Documentation: https://www.npmjs.com/package/grunt-browserify
// Compile all apps to a browser bundle

module.exports = function (grunt) {

    var browserify = grunt.config.get('browserify') || {};

    browserify.client = {
        files: {
            'src/main.bundle.js': ['src/main.js']
        }
    };

    browserify.clientWatch = {
        files: {
            'src/apps/client/main.bundle.js': ['src/main.js']
        },
        options: {
            watch: true,
            keepAlive: true
        }
    };

    grunt.config.set('browserify', browserify);
};
