// Documentation: https://github.com/gruntjs/grunt-contrib-clean
// Clear files and folders.

module.exports = function(grunt) {

    var clean = grunt.config.get('clean') || {};

    // Empties folders to start fresh
    clean = {
        dist: {
            files: [{
                dot: true,
                src: [
                    '.tmp',
                    '<%= config.dist.root %>/*',
                    '!<%= config.dist.root %>/.git*'
                ]
            }]
        }
    };

    grunt.config.set('clean', clean);

};
