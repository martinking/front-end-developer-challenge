// Documentation: https://github.com/gruntjs/grunt-contrib-copy
// Copy files and folders.

module.exports = function(grunt) {

    var copy = grunt.config.get('copy') || {};

    // Copies remaining files to places other tasks can use
    copy = {
        normalize: {
          files: [
              {
                  expand:     true,
                  cwd:        './node_modules/modularized-normalize-scss/',
                  dest:       '<%= config.dev.root %>/libs/modularized-normalize-scss/',
                  src: [
                      '**/*.scss'
                  ]
              },
          ]
        },
        client: {
            files: [
                // Core root directory files, plus images
                {
                    expand:     true,
                    dot:        true,
                    cwd:        '<%= config.dev.root %>',
                    dest:       '<%= config.dist.root %>',
                    src: [
                        'css/**/*',
                        'img/**/*',
                        '*.html',
                        'main.bundle.js',
                        'fonts/{,*/}*.*'
                    ]
                }
            ]
        }
    };

    grunt.config.set('copy', copy);

};
