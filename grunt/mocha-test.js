// Documentation - https://github.com/pghalliday/grunt-mocha-test
// Run mocha unit tests

module.exports = function(grunt) {

    var mochaTest = grunt.config.get('mochaTest') || {};

    mochaTest.client = {
        src: ['src/test/unit/**/*.spec.js']
    };

    grunt.config.set('mochaTest', mochaTest);

};
