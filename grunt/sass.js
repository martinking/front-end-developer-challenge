// Documentation: https://github.com/sindresorhus/grunt-sass
// Compile Sass to CSS

module.exports = function (grunt) {

    var sass = grunt.config.get('sass') || {};

    sass.client = {
        files: [{
            expand: true,
            cwd: 'src/scss',
            src: ['*.scss'],
            dest: 'src/css',
            ext: '.css'
        }]
    };

    grunt.config.set('sass', sass);
};
