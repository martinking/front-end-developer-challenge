// Documentation: https://github.com/gruntjs/grunt-contrib-uglify
// Minify files with UglifyJS.

module.exports = function (grunt) {

    var uglify = grunt.config.get('uglify') || {};

    uglify = {
        app: {
            options: {
                sourceMap: true,
                sourceMapName: 'dist/main.bundle.min.map'
            },
            files: {
                'dist/main.bundle.min.js': ['src/apps/client/main.bundle.js']
            }
        }
    };

    grunt.config.set('uglify', uglify);

};