var _ = require('underscore'),
    $ = require('jquery'),
    domReady = require('domready');

var App = require('./script/application');

'use strict';

domReady(function() {
    var app = new App();

    app.start(function (msg) {
        console.info(msg);
    });

});
