module.exports = function () {

    'use strict';

    return {
        start: function (callback) {
            callback('App is running!');
        }
    }
};
