module.exports = function () {

    'use strict';

    var username = 'Arqiva',
        password = 'N3wm@nStr33t';
    return {
        validate: function (usr, pass, onSuccess, onError) {
            if (usr === username && pass === password) {
                onSuccess('You are logged in.');
            } else {
                onError('Sorry, try again.');
            }
        }
    }
};
