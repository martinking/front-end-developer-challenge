require('./common.spec');

var Application = require('../../script/application');

describe('Given the application', function() {

    beforeEach(function () {
        this.application = new Application();
    });

    describe('Given the start method', function () {
        var startSpy;
        beforeEach(function () {
            startSpy = sinon.spy();
            this.application.start(startSpy);
        });

        afterEach(function (){
            startSpy.reset();
        });

        it('Should call the supplied callback with the string \'App is running!\'', function() {
            expect(startSpy.calledWith('App is running!')).to.be.true;
        });
    })
});
