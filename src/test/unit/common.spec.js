// set up a window object for libraries that need it
jsdom = require("jsdom").jsdom;
document = jsdom(undefined);
window = document.defaultView;

// jquery ajax shims
$ = require('jquery');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
$.support.cors = true;
$.ajaxSettings.xhr = function () {
    return new XMLHttpRequest;
};

// jquery ajax shims
$ = require('jquery');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
$.support.cors = true;
$.ajaxSettings.xhr = function () {
    return new XMLHttpRequest;
};

_ = require('underscore');

// handlebars template compiling
var Handlebars = require('handlebars');
var fs = require('fs');

require.extensions['.hbs'] = function (module, filename) {
    var compiled;
    var raw = fs.readFileSync(filename, 'utf8');
    compiled = Handlebars.compile(raw);
    module.exports = compiled;
};

// expectations
expect = require('chai').expect;

// sinon for spies, mocks and stubs
sinon = require('sinon');

// bootstrap all code to be covered
var glob = require('glob');

var filesToRequire = glob.sync('../src/script/**/*.js');
filesToRequire.forEach(function(file) {
    require(file.replace(/\.\/src\/script/, '../..'));
});
