require('../common.spec');

var LoginService = require('../../../script/service/login');

describe('Given the login service', function() {

    var successSpy, errorSpy;

    beforeEach(function () {
        successSpy = sinon.spy();
        errorSpy = sinon.spy();

        this.loginService = new LoginService();
    });

    afterEach(function (){
        successSpy.reset();
        errorSpy.reset();
    });

    describe('Given the login method called with valid credentials', function () {

        beforeEach(function () {
            this.loginService.validate('Arqiva', 'N3wm@nStr33t', successSpy, errorSpy);
        });

        it('Should call the onSuccess callback with the string \'You are logged in.\'', function() {
            expect(successSpy.calledWith('You are logged in.')).to.be.true;
        });

        it('Should NOT call the onError callback', function() {
            expect(errorSpy.called).to.be.false;
        });
    });

    describe('Given the login method called with invalid credentials', function () {

        beforeEach(function () {
            this.loginService.validate('INVALID', 'INVALID', successSpy, errorSpy);
        });

        it('Should call the onError callback with the string \'Sorry, try again.\'', function() {
            expect(errorSpy.calledWith('Sorry, try again.')).to.be.true;
        });

        it('Should NOT call the onSuccess callback', function() {
            expect(successSpy.called).to.be.false;
        });
    });
});
